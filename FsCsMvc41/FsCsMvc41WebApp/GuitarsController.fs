﻿namespace FsWeb.Controllers
open Repository
open System.Web.Mvc
open FsWeb.Models
open FsWeb.Repositories


 [<HandleError>]
 type GuitarsController(repository:GuitarsRepositories) =
     inherit Controller()
     new() = new GuitarsController(GuitarsRepositories())
     member this.Index()=
          use context=new FsMvcAppEntities()
          context.Guitars|>Seq.toList  |> this.View
          //repository.GetAll() |> this.View
//            use context=new FsMvcAppEntities()
//            getTop 1 context.Guitars |> Seq.toList |> this.View
      


