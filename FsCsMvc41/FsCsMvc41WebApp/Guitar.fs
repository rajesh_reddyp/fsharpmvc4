﻿namespace FsWeb.Models
 open System.ComponentModel.DataAnnotations
 open System
 
 type Guitar()=
   [<Key>] member val Id=Guid.NewGuid() with get,set
   [<Required>] member val Name="" with get,set

